# mkSilence

Removes quiet parts in audio files.

## Getting started

### Installation:

1. Install the System dependencies
    ```bash
   sudo apt-get install sox libsox-fmt-mp3
   ```
2. Make sure the executable-Flag is set for 
    ```text
   mkSilenceWatchdog.sh
   mkSilence.sh
   ```
## Usage

mkSilence.sh will remove the silence for each Input file
and add "_out.mp3" to the output file
 ```bash
mkSilence.sh input1.mp3 input2.mp3 ...
# example:  input1.mp3 -> input1.mp3_out.mp3
 ```

The same works for Input Directories 
 ```bash
mkSilence.sh inputdir1 ...
# example: inputdir1/*.mp3 inputdir1/*.mp3_out.mp3
 ```

mkSilenceWatchdog.sh scans the input directory and each subdirectory for audio files
and creates silenced versions in the output directory.

Once mkSilenceWatchdog is finished, it waits for a "COOLDOWN" period (10 seconds) and scans the input directory again to update new files.

Note: 
This script is intended to run on its own and does not need to be configured.

The default input, output directories and the "cooldown" delay are hard-coded.

Feel free to adjust the default configuration.

```bash
mkSilenceWatchdog.sh
# will use /home/<USER>/Downloads/JDownloader/ as input
# will use /home/<USER>/JDownloader_mp3/ as output
# example: ~/Downloads/JDownloader/mydir/My.m4a -> ~/JDownloader_mp3/mydir/My.m4a.mp3

mkSilenceWatchdog.sh input-dir output-dir
# example: input-dir/some-subdirectory/My.m4a -> output-dir/some-subdirectory/My.m4a.mp3
```

## License
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
