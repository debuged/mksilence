#!/bin/bash

# #########################  Installation  ###########################
#
# Installation: 
# Run the following command
# sudo apt-get install sox libsox-fmt-mp3
# in Terminal.
#
# ############################  Usage  ###############################
#
# Usage:
# Drop this Script File in a Terminal, then Drop MP3's
# you want to convert in terminal tow.
# Press Enter ....
#
# Note: Ensure the "x"-Flag is set for this, see File Properties
# in your Filemanager
#
# ############################  Config  ##############################
#
# the input directory
SRCDIR=~/Downloads/JDownloader/
# the output directory
DSTDIR=~/JDownloader_mp3/
# time to wait between each run time all directory are rescanned
COOLDOWN=10
#
# ###################################################################

INFO=false
DEBUG=false
TMPDIR=$(mktemp -d --suffix _mksilence)

# For Testing
if $DEBUG; then
  SRCDIR=~/JDownloader_in/
  DSTDIR=~/JDownloader_out/
fi

if [ $# -eq 0 ]; then
  # don't do anything, use default directories
  true
elif [ $# -eq 2 ]; then
  SRCDIR=$(realpath -m "$1")
  DSTDIR=$(realpath -m "$2")
else
  echo "Usage: mkSilenceWatchdog.sh                      - use default paths"
  echo "Usage: mkSilenceWatchdog.sh input-dir output-dir - use custom paths"
  exit 1
fi

if [ ! -d "$SRCDIR" ]; then
  echo source dir "$SRCDIR" does not exist
  exit 1
fi

if [ -e "$DSTDIR" ] && [ ! -d "$DSTDIR" ]; then
  echo "$DSTDIR" is not a directory
  exit 1
fi
mkdir -p "$DSTDIR"

JOBS=$(($(grep -c processor /proc/cpuinfo)-1))
if [ "$JOBS" -lt 1 ]; then
  JOBS=1
fi

# For Testing
# JOBS=1
# watch 'ps -aux | grep -v grep |  grep -E "sox|ffmpeg"'
$DEBUG && JOBS=1
$DEBUG && COOLDOWN=1


function run_sox {
  echo "From:" "'""$1""'"
  echo "TO:  " "'""$1"'_out.mp3'"'"
  sox "$1" "$1"'_out.mp3' silence -l 1 0.0 1% -1 1.0 1%
}

function run_sox2 {
  echo "From:" "'""$1""'"
  echo "TO:  " "'""$2""'"
  SRC="$1"
  echo
  if [[ "$SRC" == *.m4a ]]; then
    echo ------------------------------------------
    pwd
    echo "$SRC"
    convert_dir="$(dirname "$TMPDIR/$dstFile")"
    mkdir -p "$convert_dir"
    echo     ffmpeg -hide_banner -loglevel error -y -i "$SRC" "$convert_dir/$(basename "$SRC").mp3"
    ffmpeg -hide_banner -loglevel error -y -i "$SRC" "$convert_dir/$(basename "$SRC").mp3" 1>/dev/null
    SRC="$convert_dir/$(basename "$SRC").mp3"
  fi
  echo sox "$SRC" "$2" silence -l 1 0.0 1% -1 1.0 1%
  sox "$SRC" "$2" silence -l 1 0.0 1% -1 1.0 1%
}
	
function run_bg {
  while (( $(jobs | wc -l) >= JOBS )); do sleep 3; jobs; done
  run_sox2 "$1" "$2" &
}

cd "$SRCDIR"

while true; do
  date
  readarray -t srcFiles < <(find . -not -name "*.tmp.part" -name "*m4a" -o -name "*.mp3")
  # Wait for files to settle
  sleep 2
  echo -n "Files found ${#srcFiles[@]} ..."
  counter=0
  for f in "${srcFiles[@]}"
  do
    dstFile="$DSTDIR/$f"".mp3"
    if [ ! -f "$dstFile" ]; then
      counter=$((counter+1))
      $INFO && echo miss "$dstFile"
      mkdir -p "$(dirname "$dstFile")"
      run_bg "$f" "$dstFile"
    else
      $DEBUG && echo skip "$dstFile"
    fi
  done
  # rejoin all jobs
  while (( $(jobs | wc -l) >= 1 )); do
    jobs &> /dev/null
    $INFO && echo Running Jobs $(jobs | wc -l)
    sleep 4
  done
  echo -n " Files $counter Processed"
  echo
  sleep $COOLDOWN
done
}