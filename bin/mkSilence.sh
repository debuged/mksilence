#!/bin/bash

######################################################################
#
# Installation: 
# Run the following command
# sudo apt-get install sox libsox-fmt-mp3
# in Terminal.
#
# ###################################################################
#
# Usage:
# Drop this Script File in a Terminal, then Drop MP3's
# you want to convert in terminal tow. 
# Press Enter ....
#
# Note: Ensure the "x"-Flag is set for this, see File Properties
# in your Filemanager
#
######################################################################
JOBS=$(($(grep -c processor /proc/cpuinfo)-1))
if [ "$JOBS" -lt 1 ]; then
  JOBS=1
fi


function run_sox {
  echo "From:" "'""$1""'"
  echo "TO:  " "'""$1"'_out.mp3'"'"
  sox "$1" "$1"'_out.mp3' silence -l 1 0.0 1% -1 1.0 1%
  # echo "$var"
}
	
function run_bg {
  while (( $(jobs | wc -l) >= $JOBS )); do sleep 1;  done
	run_sox "$1" &
}

for var in "$@"
do
  if [ -d "$var" ]; then
    for f in "$var"/*; do
      run_bg "$f"
    done
  elif [ -f "$var" ]; then
	  run_bg "$var"
  fi
done

while (( $(jobs | wc -l) > 1 )); do
  jobs &> /dev/null
  echo Running Jobs $(jobs | wc -l)
  sleep 1
done

